# Updates to be Made

## Pre-Planning Document

The preliminary scope section is very light.  Which sections will be covered?  Which hardware?  How much will the project cost what about the project estimated timeline?

- [ ] add requirements from SRS

## Software Requirements Specification

Expand all paragraphs to 3 sentences.

- [ ] update SRS to match OOADs
	- [ ] add section 2.4.6 `Update Map Information`

## Object Oriented Analysis Diagrams

- [ ] add two usecases

