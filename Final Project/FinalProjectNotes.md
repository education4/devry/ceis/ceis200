# Final Project Notes

## Questions 

- Are appendices A (Glossary) and C (To Be Determined) intentionally absent?
- Create a Summary Evaluation of the overall quality of the software product project in the following areas:
	•	Business Analysis: <Provide details>. 
	•	Art (Graphics, Sound): <Provide details>. 
	•	User Interface: <Provide details>. 
	•	Design: <Provide details>. 
	•	Code and Programming: <Provide details>. 
	•	Overall project quality: <Provide details>.


## Deliverables

- do not zip file
- Compiled word document
- .mpp file
- do not upload .vsdx
- team summary journal
- do not turn in individual journals
- 20 - 30 slides with narration
	- summary of individual evaluations (no more than 2 slides per member)
	- summary of project level evaluations (one for each slide; 3 total)
		- SRS (Adrian)
		- OOADs (Adrian)
		- Test Cases (Ramon)
	- OOADs (one slide for each; 4 slides total)
		- [ ] class diagram (mehdi)
		- [ ] hierarchal state machine (mehdi)
		- [ ] update map sequence diagram (bryan)
		- [ ] other sequence diagram (bryan)
	- A sample risk matrix and explain the purpose (one slide) (Adrian)
	- overview of your test plan (one to two slides) (Ramon)
	- one test case (one slide) (Ramon)
	- summary of individual evaluations; only item #3
	
	