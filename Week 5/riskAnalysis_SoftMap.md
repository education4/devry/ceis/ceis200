# SoftMap Risk Analysis

## Risk list

|id| condition | consequence |
|:-:||:----------| :---------- |
|1|lead desginer leaves| team is forced to authorize and prioritize tasks without guidance; team may be unprepared|
|2|power outage during development | extension of project length; potential loss of completed work|
|3|over budget| mall cuts funding; system may be implemented in an incomplete state leading to mall patron disgruntlement|
|4|software bugs| poor patron experience; extension of project length|
|5|new feature requests| obfuscation of scope and purpose; lack of funding;|
|6|shortened deadlines| employee crunch; poor QA|
|7|software lacks compatibility with hardware| either cost of acquiring new hardware or developing new software|
|8|poor implementation of disability features| exclusion of the visually impared;
|9|use of culturally dependant iconography| significant percentage of mall patrons are unable to effectively use SoftMap; may lead to feeling isolated from mall community|

## probability & impact

|id| probability| impact| response|
|:---:| :------| :------| :-----|
|1| unlikely | short term; loss of productivity | assign team member coordination responsibilites |
|2|rare | protracted outage unlikely; work is stored on non-volatile media| all devices dependant on an outlet will have a UPS to provide opportunity to move work to non-volatile media|
|3| rare| work to be completed and in progress | cut features to reduce development and QA costs
|4| unlikely| money; public perception; entire project; unknown length;| recall or disable units; redevelop software|
|5| likely | entire project | discuss with client to discern critical features; explain budget and time effects of requests|
|6| likely | employee wellbeing; product cohesiveness | negotiate for more budget to hire new developers and for more time to integrate developers; or cut features|
|7| rare | half of project; impacts costs| 
|8| likely | release & redevelopment| disable features until they have been reworked to meet needs|
|9| likely | apologize and explain situation; seek and integrate culturally diverse ideas|

## prevention & monitoring

|id| prevention| monitoring|
|:-:| :------- | : ---- |
|1| encourage team members to communicate with one another; engage all members with client needs and productivity metrics| ! MONITORING !
|2| do not allow team members to bring power hungry devices such as space heaters and toaster ovens; record which outlets are on which circuits | check circuits for expected power consumption|
|3| keep a weekly running total on costs and compare with estimations; | quarterly or biannual independent audit?
|4| invest in QA | implement limited release with monitoring to ensure no problems arise|
|5| regularly communicate with client and provide updates on how system is meeting user needs| ! MONITORING ! |
|6| repeat 5
|7| check hardware specifications; communicate needs with supplier;| early testing of software on a small number of units|
|8| reach out to and involve %disability organization% | redo 4, but target disabled population
|9| redo 8, but with different cultural organizations | redo 4, but culturally targeted