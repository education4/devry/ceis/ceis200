# Week 5 ToDo

## minimum requirements

- [] use case specification 1 (Anna)
- [] sequence diagram 1 (Anna)
- [] use case specification 2 (Bryan)
- [] sequence diagram 2 (Bryan)
- [] class diagram (Mehdi)
- [] state machine diagram
- [] risk analysis matrix (Adrian & Ramon)
	- [x] reasearch RAMs
- [x] add system boundary to use case diagram

## post completion

- [] evaluation (Adrian & Ramon)
- [] verify consistency within Obeject Oriented Analysis Diagrams (OOAD) (Adrian)
- [] update WBS to match OOADs (Adrian)
- [] update SRS to match OOADs (Adrian)

## extra

- [] add accessibility alternate flows (Adrian)
	- [] notate [Access overlays: improving non-visual access to large touch screens for blind users](https://dl.acm.org/doi/10.1145/2047196.2047232) (Adrian)
	- [] look for more research (Adrian)
- [] specifiy all use cases (Adrian)

